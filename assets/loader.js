function logTerminal(message) {
  const terminal = document.getElementById('loading-term');
  terminal.innerHTML += message + '<br>';
  terminal.scrollTop = terminal.scrollHeight;
}

function logRedBoldTerminal(message) {
  const terminal = document.getElementById('loading-term');
  terminal.innerHTML += '<span style="color:red;"><b>' + message + '</b></span><br>';
  terminal.scrollTop = terminal.scrollHeight;
}

logTerminal("doxr custom loader (so the loader is less boring)\n");

function waitAndLog(message, delay) {
  setTimeout(function() {
      logTerminal(message);
  }, delay);
}

window.addEventListener('deviceorientation', function(event) {
  if (event.alpha !== null) logTerminal('DEVICEORIENTATION UPDATE: ' + event.alpha + ' ' + event.beta + ' ' + event.gamma);
});

waitAndLog(navigator.userAgent, 50);
waitAndLog('Preferred Language: ' + navigator.language, 100);
waitAndLog(`Screen: ${screen.width} x ${screen.height} || Available Screen: ${screen.availWidth} x ${screen.availHeight} || Color Depth: ${screen.colorDepth}`, 150);

setTimeout(function() {
  navigator.getBattery()
      .then(r => {
          const batteryPercentage = Math.round(r.level * 100);
          logTerminal('Battery percentage: ' + batteryPercentage);
      })
      .catch(error => {
          logTerminal("Error accessing battery status:" + error);
      });
}, 200);

waitAndLog('Device Memory: ' + navigator.deviceMemory + 'GBs', 250);

async function completeload() {
  logRedBoldTerminal("Click anywhere to begin.");

  document.addEventListener('click', function() {
      const terminal = document.getElementById('loading-term');
      terminal.innerHTML = '';
      terminal.remove();
      document.removeEventListener('click', arguments.callee);
      play('song');
      var playPauseIcon = document.getElementById('songPlayPause');
  });
}

window.onload = completeload;
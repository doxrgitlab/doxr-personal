var activeSong;

function play(id) {
 activeSong = document.getElementById(id);
 activeSong.play();

 var percentageOfVolume = activeSong.volume;
 var percentageOfVolumeMeter = document.getElementById('volumeMeter').offsetWidth * percentageOfVolume;
 document.getElementById('volumeStatus').style.width = Math.round(percentageOfVolumeMeter) + "px";

 activeSong.addEventListener('ended', function() {
    this.play();
 });
}

function pause() {
 activeSong.pause();
}

function playPause(id) {
 activeSong = document.getElementById(id);
 var playPauseIcon = document.getElementById('songPlayPause');

 if (activeSong.paused) {
    activeSong.play();
    playPauseIcon.className = 'fas fa-pause';
 } else {
    activeSong.pause();
    playPauseIcon.className = 'fas fa-play';
 }
}

function setVolume(percentage) {
 activeSong.volume = percentage / 100;

 var percentageOfVolume = activeSong.volume;
 var percentageOfVolumeSlider = document.getElementById('volumeMeter').value * percentageOfVolume;
 document.getElementById('volumeStatus').style.width = Math.round(percentageOfVolumeSlider) + "px";
}

function setNewVolume(obj, e) {
 var volumeSliderWidth = obj.offsetWidth;
 var evtobj = window.event ? event : e;
 var clickLocation = evtobj.layerX - obj.offsetLeft;

 var percentage = (clickLocation / volumeSliderWidth);
 setVolume(percentage);
}